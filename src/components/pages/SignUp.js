import React,{useState, useEffect}from 'react';
import {Redirect} from 'react-router-dom';
import './style.css';
import { config } from '../../config';
import {saveUser} from '../api/userApi';


//8 to 40
 class SignUp extends React.Component {
      constructor(props){
          super(props);
          this.state = {
              redirect: false,
              error: null
          }
          this.userName = "";
          this.email = "";
          this.password = "";
      }
      onChangeText(type, text){
          this[type] = text;
      }

      onSubmitForm = ()=>{
         
         let data = {
             userName: this.userName,
             email: this.email,
             password: this.password
         } 
         console.log(data);
         saveUser(data)
             .then((response)=>{
                 console.log(response)
                 if(response.status === 200) {
                     this.setState({redirect: true})
                 } else {
                     this.setState({error: 'un problème s\'est produit durant l\'utilisateur !'})
                 }
             })
     }
     render(){


  
    return (
        <div className="base-container ">
          {this.state.redirect && <Redirect to="/login" />}
          {this.state.error !== null && <p style={{color: "red"}}>{this.state.error}</p>}
          <div className="head">Sign Up</div>
          <div className="content">
            <form className="form"
              onSubmit={(e)=>{
                  e.preventDefault();
                  this.onSubmitForm();
              }
            }>
              <div className="form-group">
                <label htmlFor="username">Username</label>
                <input 
                  type="text" 
                  name="username" 
                  placeholder="username" 
                  onChange={(e)=>{
                    this.onChangeText('userName', e.currentTarget.value)
                  }}
                />
              </div>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input 
                  type="text" 
                  name="email" 
                  placeholder="email" 
                  onChange={(e)=>{
                    this.onChangeText('email', e.currentTarget.value)
                  }}
                />
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input 
                  type="password" 
                  name="password" 
                  placeholder="password" 
                  onChange={(e)=>{
                    this.onChangeText('password', e.currentTarget.value)
                  }}
                />
              </div>
              <div className="foot">
                <button type="submit" className="btn">
                  Sign Up
                </button>
              </div>
            </form>
          </div>
          
        </div>
      );
    }
  }

  export default SignUp
   
    



  