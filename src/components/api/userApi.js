import axios from 'axios';
import {config} from '../../config';
const token = window.localStorage.getItem('adventure');
export const saveUser = (data)=>{
    return axios.post(config.api_url+'/api/v1/user/save', data)
            .then((res)=>{
               return res.data 
            })
            .catch((err)=>{
                return err
            })
}

export const loginUser = (data)=> {
    return axios.post(config.api_url+'/api/v1/user/login', data)
            .then((res)=>{
               return res.data 
            })
            .catch((err)=>{
                return err
            })
}


