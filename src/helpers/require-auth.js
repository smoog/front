import React from 'react';
import {Redirect} from 'react-router-dom';
import {config} from '../config';
import axios from 'axios';
import {connect} from 'react-redux';
import {loadUserInfo} from '../actions/user/userAction';

export default function(ChildComponent, withAuth = false) {
    
    class RequireAuth extends React.Component {
        constructor(props) {
            super(props)
            this.state = {
                redirect: false
            }
        }
        
        componentDidMount() {
            
            const token = window.localStorage.getItem('adventure-token');
            console.log(token);
            if(token === null && withAuth) {
                console.log('redirect 1');
                this.setState({redirect: true});
            } else {
                if(this.props.user.isLogged === false) {
                    axios.get(config.api_url+'/api/v1/checkToken', {headers: {'x-access-token': token}})
                    .then((response)=>{
                        console.log(response);
                        if(response.data.status !== 200) {
                            if(withAuth === true) {
                                this.setState({redirect: true});
                            }
                        } else {
                            console.log('connecté');
                            this.props.loadUserInfo(response.data.user);
                        }
                    })
                }
                
            }
        }
        
        render() {
            if(this.state.redirect) {
                console.log('redirect');
                return <Redirect to="/Login" />
            }
            return (<ChildComponent {...this.props} />)
        }
        
    }
    
    const mapDispatchToProps = {
        loadUserInfo
    }
    
    const mapStateToProps = (store)=>{
        return {
            user: store.user
        }
    }
    
    
    return connect(mapStateToProps, mapDispatchToProps)(RequireAuth);
}