import React from 'react'
import CardItem from './Carditem'
import './Cards.css';

function Cards(){
    return(
        <div className='cards'>
            <h1>Check out these EPIC Destinations!</h1>
            <div className="cards__container">
                <div className="cards__wrapper">
                    <ul className="cards__items">
                        <CardItem
                        src="../images/img-9.jpg" 
                        text="Explore the hidden waterfall deep
                        inside the Amazon Jungle"
                        label='Adventure'//titre de la photo
                        path='/services'
                        />
                        <CardItem
                        src="images/img-5.jpg" 
                        text="Stay with confort in Amazing Hotels"
                        label='Luxury'//titre de la photo
                        path='/services'
                        />
                    </ul>
                    <ul className="cards__items">
                        <CardItem
                        src="images/img-8.jpg" 
                        text="Explore the hidden path into the desert of Morocco
                        "
                        label='Adventure'//titre de la photo
                        path='/services'
                        />
                        <CardItem
                        src="images/img-3.jpg" 
                        text="Travel through the Sea
                        in a Private Cruise"
                        label='Sealing'//titre de la photo
                        path='/services'
                        />
                         <CardItem
                        src="images/img-4.jpg" 
                        text="Travel through the Islands of Football"
                        label='Sport'//titre de la photo
                        path='/services'
                        />
                    </ul>
                </div>

            </div>
        </div>
    )
}

export default Cards;