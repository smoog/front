import React from 'react';
import Navbar from './components/Navbar';
import './App.css';
import Home from './components/pages/Home';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Services from './components/pages/Services';
import Products from './components/pages/Products';
import SignUp from './components/pages/SignUp';
import Login from './components/pages/Login';
import Logout from './components/pages/Logout';
import RequireAuth from './helpers/require-auth';

function App() {
  return (

      <Router>
        <Navbar />
        <Switch>
          <Route path='/' exact component={RequireAuth(Home)} />
          <Route path='/services' component={RequireAuth(Services,true)} />
          <Route path='/products' component={RequireAuth(Products)} />
          <Route path='/Sign-up' component={SignUp} />
          <Route path='/Login' component={Login}/>
          <Route path="/Logout" component={Logout} />

        </Switch>
      </Router>

  );
}

export default App;